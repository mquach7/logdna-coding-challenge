﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

using Newtonsoft.Json;

using Coding_Challenge.Model;

namespace Coding_Challenge
{
    public class Program : ApiController
    {
        private PropertyStack SetPropertyStack(string input)
        {
            PropertyStack ps = new PropertyStack();
            if (input[0].Equals('>'))
                ps.GreaterThan = SetProperty(input.Substring(1));
            else if (input[0].Equals('<'))
                ps.LessThan = SetProperty(input.Substring(1));
            else if (input[0].Equals('='))
                ps.EqualTo = SetProperty(input.Substring(1));

            return ps;
        }

        private Property SetProperty(string input)
        {
            Property prop = new Property();
            string value = input;
            if ((input.IndexOf(">") != -1) || (input.IndexOf("<") != -1) || (input.IndexOf("=") != -1))
                value = input.Remove(0, 1);

            if (input.IndexOf(">") != -1)
            {
                prop.GreaterThan = value;
            }
            else if (input.IndexOf("<") != -1)
            {
                prop.LessThan = value;
            }
            else if (input.IndexOf("=") != -1)
            {
                prop.EqualTo = value;
            }
            else if (input.IndexOf("\"") != -1)
            {
                Char[] array = new char[] { '\"' };
                value = value.Split(array)[1];
                prop.Quote = value;
            }
            else if (input.IndexOf("len(") != -1)
            {
                value = value.Replace("len", "");
                value = value.Replace("(", "");
                value = value.Replace(")", "");
                prop.Length = value;
            }
            return prop;
        }

        [HttpPost]
        public string CreatJson(string input)
        {
            bool bracketAnd = true;
            bool andSearch = true;
            int complexity = 0;
            string bracketPattern = "\\([^\\d]*\\)";
            Regex bracketCheck = new Regex(bracketPattern);
            string[] matches;

            if (bracketCheck.IsMatch(input))
            {
                bracketPattern = "(?<=^[^\\(]*(?:\\([^\\(]*\\)[^\\(]*)*) (?=(?:[^\\)]*\\)[^\\)]*\\))*[^\\)]*$)";
                string[] bracketMatches = bracketCheck.Split(input);
                BracketSearch bsearch = new BracketSearch();
                BracketSearchProperty bPropSearch = new BracketSearchProperty();
                BracketSearchPropertyStack bStacked = new BracketSearchPropertyStack();

                if (bracketMatches.Count() > 0)
                {
                    char[] chars = { '>', '<', '=' };
                    foreach (var match in bracketMatches)
                    {
                        if (String.Compare(match.ToUpper(), "OR") == 0)
                            bracketAnd = false;

                        if ((match.IndexOf(">=") != -1) || (match.IndexOf("=<") != -1) || (match.IndexOf("=\"") != -1) || (match.IndexOf(">len") != -1) || (match.IndexOf("<len") != -1) || (match.IndexOf("=len") != -1))
                            complexity = 2;
                        else if (match.IndexOfAny(chars) > -1)
                            complexity = 1;
                    }
                }

                string pattern = "(?<=^[^\"]*(?:\"[^\"]*\"[^\"]*)*) (?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";
                Regex rgx = new Regex(pattern);

                foreach (var bracketMatch in bracketMatches)
                {
                    matches = rgx.Split(bracketMatch);
                    Search search = new Search();
                    SearchProperty complexSearch = new SearchProperty();
                    SearchPropertyStack stacked = new SearchPropertyStack();

                    if ((String.Compare(bracketMatch.ToUpper(), "OR") == 0) || (String.Compare(bracketMatch.ToUpper(), "AND") == 0))
                        continue;

                    foreach (var match in matches)
                    {
                        Property prop = new Property();

                        if ((String.Compare(match.ToUpper(), "OR") == 0) || (String.Compare(match.ToUpper(), "AND") == 0))
                            continue;

                        switch (complexity)
                        {
                            case 0:
                                if (match.IndexOf("!") != -1)
                                {
                                    int index = match.IndexOf("!");
                                    search.Not = (match.Remove(index, 1).ToUpper() == "TRUE") ? true : false;
                                }
                                else
                                {
                                    if (andSearch)
                                    {
                                        if (search.And == null)
                                            search.And = new List<string>();
                                        search.And.Add(match);
                                    }
                                    else
                                    {
                                        if (search.Or == null)
                                            search.Or = new List<string>();
                                        search.Or.Add(match);
                                    }
                                }
                                break;

                            case 1:
                                if (andSearch)
                                {
                                    if (complexSearch.AndMultiProperty == null)
                                        complexSearch.AndMultiProperty = new List<Property>();

                                    complexSearch.AndMultiProperty.Add(SetProperty(match));
                                }
                                else
                                {
                                    if (complexSearch.OrMultiProperty == null)
                                        complexSearch.OrMultiProperty = new List<Property>();

                                    complexSearch.OrMultiProperty.Add(SetProperty(match));
                                }
                                break;

                            case 2:
                                if (andSearch)
                                {
                                    if (stacked.AndMultiProperty == null)
                                        stacked.AndMultiProperty = new List<PropertyStack>();

                                    stacked.AndMultiProperty.Add(SetPropertyStack(match));
                                }
                                else
                                {
                                    if (stacked.OrMultiProperty == null)
                                        stacked.OrMultiProperty = new List<PropertyStack>();

                                    stacked.OrMultiProperty.Add(SetPropertyStack(match));
                                }
                                break;
                        }
                    }

                    switch(complexity)
                    {
                        case 0:
                            if (bracketAnd)
                            {
                                if (bsearch.And == null)
                                    bsearch.And = new List<Search>();

                                bsearch.And.Add(search);
                            }
                            else
                            {
                                if (bsearch.Or == null)
                                    bsearch.Or = new List<Search>();

                                bsearch.Or.Add(search);
                            }
                            break;

                        case 1:
                            if (bracketAnd)
                            {
                                if (bPropSearch.And == null)
                                    bPropSearch.And = new List<SearchProperty>();

                                bPropSearch.And.Add(complexSearch);
                            }
                            else
                            {
                                if (bPropSearch.Or == null)
                                    bPropSearch.Or = new List<SearchProperty>();

                                bPropSearch.Or.Add(complexSearch);
                            }
                            break;
                        case 2:
                            if (bracketAnd)
                            {
                                if (bStacked.And == null)
                                    bStacked.And = new List<SearchPropertyStack>();

                                bStacked.And.Add(stacked);
                            }
                            else
                            {
                                if (bStacked.Or == null)
                                    bStacked.Or = new List<SearchPropertyStack>();

                                bStacked.Or.Add(stacked);
                            }
                            break;
                    }
                }

                if (complexity == 1)
                    return JsonConvert.SerializeObject(bPropSearch);
                else if (complexity == 2)
                    return JsonConvert.SerializeObject(bStacked);
                return JsonConvert.SerializeObject(bsearch);
            }
            else
            {
                string pattern = "(?<=^[^\"]*(?:\"[^\"]*\"[^\"]*)*) (?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";
                Regex rgx = new Regex(pattern);
                matches = rgx.Split(input);
                Search search = new Search();
                SearchProperty complexSearch = new SearchProperty();
                SearchPropertyStack stacked = new SearchPropertyStack();

                if (matches.Count() > 0)
                {
                    char[] chars = { '>', '<', '=' };
                    foreach (var match in matches)
                    {
                        if (String.Compare(match.ToUpper(), "OR") == 0)
                            andSearch = false;

                        if ((match.IndexOf(">=") != -1) || (match.IndexOf("=<") != -1) || (match.IndexOf("=\"") != -1) || (match.IndexOf(">len") != -1) || (match.IndexOf("<len") != -1) || (match.IndexOf("=len") != -1))
                            complexity = 2;
                        else if (match.IndexOfAny(chars) > -1)
                            complexity = 1;
                    }
                }

                foreach (var match in matches)
                {
                    Property prop = new Property();

                    if ((String.Compare(match.ToUpper(), "OR") == 0) || (String.Compare(match.ToUpper(), "AND") == 0))
                        continue;

                    switch (complexity)
                    {
                        case 0:
                            if (match.IndexOf("!") != -1)
                            {
                                int index = match.IndexOf("!");
                                search.Not = (match.Remove(index, 1).ToUpper() == "TRUE") ? true : false;
                            }
                            else
                            {
                                if (andSearch)
                                {
                                    if (search.And == null)
                                        search.And = new List<string>();
                                    search.And.Add(match);
                                }
                                else
                                {
                                    if (search.Or == null)
                                        search.Or = new List<string>();
                                    search.Or.Add(match);
                                }
                            }
                            break;

                        case 1:
                            if (andSearch)
                            {
                                if (complexSearch.AndMultiProperty == null)
                                    complexSearch.AndMultiProperty = new List<Property>();

                                complexSearch.AndMultiProperty.Add(SetProperty(match));
                            }
                            else
                            {
                                if (complexSearch.OrMultiProperty == null)
                                    complexSearch.OrMultiProperty = new List<Property>();

                                complexSearch.OrMultiProperty.Add(SetProperty(match));
                            }
                            break;

                        case 2:
                            if (andSearch)
                            {
                                if (stacked.AndMultiProperty == null)
                                    stacked.AndMultiProperty = new List<PropertyStack>();

                                stacked.AndMultiProperty.Add(SetPropertyStack(match));
                            }
                            else
                            {
                                if (stacked.OrMultiProperty == null)
                                    stacked.OrMultiProperty = new List<PropertyStack>();

                                stacked.OrMultiProperty.Add(SetPropertyStack(match));
                            }
                            break;
                    }
                }

                if (complexity == 1)
                    return JsonConvert.SerializeObject(complexSearch);
                else if (complexity == 2)
                    return JsonConvert.SerializeObject(stacked);
                return JsonConvert.SerializeObject(search);
            }
        }

        //static void Main(string[] args)
        //{
        //    Console.WriteLine(Search("bob or fred"));
        //    Console.WriteLine(Search(">400 <500"));
        //    Console.WriteLine(Search("!false"));
        //    Console.WriteLine(Search("error OR info"));
        //    Console.WriteLine(Search("=\"TEST DATA\" OR >len(9)"));
        //    Console.WriteLine(Search("(fred and bob) AND (patty or millie)"));
        //    System.Diagnostics.Debug.WriteLine("test");
        //}
    }
}
