﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Coding_Challenge.Model
{
    public class SearchPropertyStack
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "$and")]
        public List<PropertyStack> AndMultiProperty { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "$or")]
        public List<PropertyStack> OrMultiProperty { get; set; }
    }
}
