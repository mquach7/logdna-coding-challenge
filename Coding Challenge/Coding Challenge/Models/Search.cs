﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Coding_Challenge.Model
{
    public class Search
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "$and")]
        public List<string> And { get; set; }
        
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "$or")]
        public List<string> Or { get; set; }
        
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "$not")]
        public bool? Not { get; set; }
    }
}
