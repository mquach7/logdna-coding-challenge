﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Coding_Challenge.Model
{
    public class PropertyStack
    {
        [JsonProperty(PropertyName = "$gt", NullValueHandling = NullValueHandling.Ignore)]
        public Property GreaterThan { get; set; }

        [JsonProperty(PropertyName = "$lt", NullValueHandling = NullValueHandling.Ignore)]
        public Property LessThan { get; set; }

        [JsonProperty(PropertyName = "$eq", NullValueHandling = NullValueHandling.Ignore)]
        public Property EqualTo { get; set; }
    }
}
