﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Coding_Challenge.Model
{
    public class BracketSearchPropertyStack
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "$and")]
        public List<SearchPropertyStack> And { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "$or")]
        public List<SearchPropertyStack> Or { get; set; }
    }
}
