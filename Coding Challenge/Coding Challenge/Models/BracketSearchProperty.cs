﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Coding_Challenge.Model
{
    public class BracketSearchProperty
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "$and")]
        public List<SearchProperty> And { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "$or")]
        public List<SearchProperty> Or { get; set; }
    }
}
