﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Coding_Challenge.Model
{
    public class Property
    {
        [JsonProperty(PropertyName = "$gt", NullValueHandling = NullValueHandling.Ignore)]
        public string GreaterThan { get; set; }      

        [JsonProperty(PropertyName = "$lt", NullValueHandling = NullValueHandling.Ignore)]
        public string LessThan { get; set; }

        [JsonProperty(PropertyName = "$eq", NullValueHandling = NullValueHandling.Ignore)]
        public string EqualTo { get; set; }

        [JsonProperty(PropertyName = "$len", NullValueHandling = NullValueHandling.Ignore)]
        public string Length { get; set; }

        [JsonProperty(PropertyName = "$quoted", NullValueHandling = NullValueHandling.Ignore)]
        public string Quote { get; set; }
    }
}
