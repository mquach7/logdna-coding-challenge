﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;

namespace Coding_Challenge.Model
{
    public class BracketSearch
    {
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "$and")]
        public List<Search> And { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore, PropertyName = "$or")]
        public List<Search> Or { get; set; }
    }
}
