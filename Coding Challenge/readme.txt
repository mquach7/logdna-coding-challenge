Assumptions:
1) The level of "complexity" for one or more criteria of a search will be all the same. Example being for the case >400 <500, the complexity would be 2 (one for the and and another for the greater than or less than symbol) and
is assumed that something like >400 =<500 is not valid. 
2) The symbols such as >= or =< will be presented as two different properties (gt and eq or eq and lt)
3) Brackets do not require a property within the json itself
